function xetKhuVuc(khuVuc) {
  if (khuVuc == "X") {
    return 0;
  }

  if (khuVuc == "A") {
    return 2;
  }

  if (khuVuc == "B") {
    return 1;
  }

  if (khuVuc == "C") {
    return 0.5;
  }
}

function xetDoiTuong(doiTuong) {
  if (doiTuong == "0") {
    return 0;
  }

  if (doiTuong == "1") {
    return 2.5;
  }

  if (doiTuong == "2") {
    return 1.5;
  }

  if (doiTuong == "3") {
    return 1;
  }
}

function xuatKetQua() {
  var khuVucValue = document.getElementById("dd-khu-vuc").value;
  var doiTuongValue = document.getElementById("dd-doi-tuong").value;
  var diemChuanValue = document.getElementById("txt-diem-chuan").value * 1;
  var diemMon1Value = document.getElementById("txt-mon1").value * 1;
  var diemMon2Value = document.getElementById("txt-mon2").value * 1;
  var diemMon3Value = document.getElementById("txt-mon3").value * 1;
  var ketQuaEl = document.getElementById("txt-ket-qua");
  var diemKhuVuc = xetKhuVuc(khuVucValue);
  var diemDoiTuong = xetDoiTuong(doiTuongValue);
  var tongDiem, result;

  tongDiem =
    diemMon1Value + diemMon2Value + diemMon3Value + diemKhuVuc + diemDoiTuong;

  if (diemMon1Value == 0 || diemMon2Value == 0 || diemMon3Value == 0) {
    result = "rớt do có điểm 0";
  } else {
    if (tongDiem >= diemChuanValue) {
      result = "đậu";
    } else {
      result = "rớt";
    }
  }

  ketQuaEl.innerText = `Kết quả: Bạn đã ${result}. Tổng điểm của bạn: ${tongDiem}`;
}
