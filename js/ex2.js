const giaSo1Den50 = 500;
const giaSo51Den100 = 650;
const giaSo101Den200 = 850;
const giaSo201Den350 = 1100;
const giaTren350 = 1300;

function tinhTienDien() {
  var tenValue = document.getElementById("txt-ten").value;
  var soDienValue = document.getElementById("txt-kw").value * 1;
  var tienSo1Den50 = giaSo1Den50 * 50;
  var tienSo51Den100 = giaSo51Den100 * 50;
  var tienSo101Den200 = giaSo101Den200 * 100;
  var tienSo201Den350 = giaSo201Den350 * 150;
  var result;
  var ketQuaEl = document.getElementById("txt-ket-qua");

  if (soDienValue <= 50) {
    result = soDienValue * giaSo1Den50;
  } else if (soDienValue <= 100) {
    result = tienSo1Den50 + (soDienValue - 50) * giaSo51Den100;
  } else if (soDienValue <= 200) {
    result =
      tienSo1Den50 + tienSo51Den100 + (soDienValue - 100) * giaSo101Den200;
  } else if (soDienValue <= 350) {
    result =
      tienSo1Den50 +
      tienSo51Den100 +
      tienSo101Den200 +
      (soDienValue - 200) * giaSo201Den350;
  } else {
    result =
      tienSo1Den50 +
      tienSo51Den100 +
      tienSo101Den200 +
      tienSo201Den350 +
      (soDienValue - 350) * giaTren350;
  }

  ketQuaEl.innerHTML = `Hế lô ${tenValue}. Tiền điện của ${tenValue} là <span class="text-danger">${result} đồng</span>`;
}
